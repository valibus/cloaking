# CLOAKING #

This project has a sharing purpose of cloaking management to help search engine bot to better crawl website and avoif crawlbudget problem.

### What is this repository for? ###

* Quick summary 
* Version 0.6 (BETA)
* author : Patrick Valibus
* website : [https://www.410-gone.fr](https://www.410-gone.fr)

### How do I get set up? ###

* Summary of set up
You should download the code and put it on your server. Enabling the code will depend on your framework. 
* Configuration
Enable the cron management to repopulate the file listeIp.php
your contab should louk like something like this:
*\ \* \* \* \* php /directory-tocloacking/cron.php

* Dependencies
Your apache/php user should be able to write in cloacking directory, create read update and delete files
* Database configuration
You should enable access to your database to frequently check the ipAddress
database configuration could be done in database/connect.php

* How to run tests
2 types of tests:
- changing the user agent in google chrome
- forcing the remote Ip address

A list of test are set in : testCloaking.php
* Deployment instructions

## Changelog ##
*0.6-beta
Bug fix performance when reading temp cache file

*0.5-beta
Add strict comparaison to prevent from bug inarray with cache

*0.4-beta
Replace in array by isset to improve perf. testing ok

*0.4-alpha
Replace in array by isset to improve perf. still need testing

*0.3-beta
Fix version with missing connect.php file

*0.2-beta
Framework has now a real production environment packaging

*0.1-beta
First version for beta test (draft)