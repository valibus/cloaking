<?php

/**
 * Created by PhpStorm.
 * User: pvali
 * Date: 28/04/2017
 * Time: 15:28
 */
ini_set('display_errors', 1);
require_once(__DIR__ . "/framework/framework.php");
//verbose mode / debug
//set 2nd parameter to true to display return from action
$refresh = new ipList($linkDb, false);
if ($refresh->isDebug())
    echo "Start gen new ip file<br/>";
if ($refresh->isDebug())
    echo "Start refresh table<br/>";

if ($refresh->isDebug())
    echo "Start adding temp file to DB<br/>";
if($refresh->isTempFileExisting()){
    $refresh->loadTempArray();
        if(count($refresh->_tempArray)>0)
            foreach($refresh->_tempArray as $newIp => $value){
                if (!filter_var(trim($newIp), FILTER_VALIDATE_IP) === false) {
                    $refresh->addItemToDb(trim($newIp), "cron update", "google");
                }
            }
    unlink($refresh->getTempFileUri());
}

$refresh->refreshTable();

if ($refresh->hasToBeUpdated()) {
    if ($refresh->isDebug())
        echo "Start gen new ip file";
    $refresh->genFile();
    if ($refresh->isDebug())
        echo "END gen new ip file";
} else {
    if ($refresh->isDebug())
        echo "file didn't need to be updated<br />";
}

