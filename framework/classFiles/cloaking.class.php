<?php
/**
 * Created by PhpStorm.
 * User: pvali
 * Date: 27/04/2017
 * Time: 20:26
 */
class cloaking
{
    /**
     *
     */
    const USER_AGENT_MASK = "/Googlebot/";
    /**
     *
     */
    const DNS_NAME_MASK ="/googlebot/";
    public $_isGoogle;
    protected $_log;
    public $_remoteIp;
    public $_userAgent;
    public $_reverseIp;
    public $_reverseName;


    /**
     * cloaking constructor.
     * @param $adresseIp
     * @param $userAgent
     * @param bool $log
     */
    public function __construct($adresseIp=false, $userAgent=false, $log=false){
        $this->_isGoogle=false;
        $this->_remoteIp=$adresseIp;
        $this->_userAgent=$userAgent;
        $this->_log=$log;
    }

    /**
     * @param $userAgent
     * @return bool
     */
    private function checkUserAgent($userAgent){
        preg_match(self::USER_AGENT_MASK, $userAgent, $matchesUAgent, PREG_OFFSET_CAPTURE);
        if(count($matchesUAgent)>0){
            return true;
        }else{
            return false;
        }
    }
    /**
     * @param $userAgent
     * @return bool
     */
    public function checkDnsName($dnsName){
        preg_match(self::DNS_NAME_MASK, $dnsName, $matchesDnsName, PREG_OFFSET_CAPTURE);
        if(count($matchesDnsName)>0){
            return true;
        }else{
            return false;
        }
    }

    /**
     *
     */
    public function getDnsName($remoteIp=false){
        if($remoteIp===false)
            $remoteIp=$this->_remoteIp;
        $this->_reverseName=gethostbyaddr($remoteIp);
        return $this->_reverseName;
    }
    /**
     *
     */
    protected function getUserAgent($userAgent=false){
        if($userAgent===false)
            $userAgent=$this->_userAgent;
        return $userAgent;
    }

    /**
     *
     */
    private function getReverseIp(){
        $this->_reverseIp=gethostbyname($this->_reverseName);
    }
    private function readTempFile(){

    }
    public function checkIfCloakingOk($cache=true,$linkDb=false,$arrayContent=false){
        if($cache==true){
            $check=new ipList($linkDb);
            if($check->isTempFileExisting()){
                $check->loadTempArray();
                if($check->isInTempArray($this->_remoteIp)) {
                    return true;
                }
            }

            $check->loadArray($arrayContent);
            if($check->isInArray($this->_remoteIp)){
                return true;
            }
            else{
                if ($this->checkUserAgent($this->getUserAgent())) {
                    if($this->checkDnsName($this->getDnsName())){
                        $check->addToTempFile($this->_remoteIp);
                        return true;
                    }else{
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }else {
            if ($this->checkUserAgent($this->getUserAgent())) {
                if ($this->checkDnsName($this->getDnsName())) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
}