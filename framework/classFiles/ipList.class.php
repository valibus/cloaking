<?php

/*
 * Created by PhpStorm.
 * User: pvali
 * Date: 28/04/2017
 * Time: 09:28
 */

class ipList
{
    const CLOAKED_IP_TABLE = "cloaked_ip";
    const LIST_IP_FILENAME = "listeIp.php";
    const LIST_IP_TEMP_FILE = "tempIp.txt";
    const CACHE_FILES_DIRECTORY = "cacheFiles";
    const REFRESH_DAYS_PERIOD = 10;
    const DEFAULT_POOL = 10;
    private $debug;
    private $newInformation;
    private $_linkDb;
    private $_currentArray;
    public $_tempArray;

    public function __construct($linkDb, $debug = false)
    {
        $this->_linkDb = $linkDb;
        $this->debug = $debug;
        $this->newInformation = false;
    }

    /**
     * @param $host
     * @param $origine
     * @param $botName
     * @param null $dnsName
     */
    public function addItemToDb($host, $origine, $botName, $status = 1, $dnsName = null)
    {
        if ($this->_linkDb->query("
          INSERT INTO " . self::CLOAKED_IP_TABLE . " (`id`, `ip_address_v4`, `dns_name`, `date_discover`, `date_update`, `last_check`, `status`, `bot`, `origin`)
          VALUES (NULL, '" . $host . "', '" . $dnsName . "', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '2017-01-01 00:00:00', '" . $status . "', '" . $botName . "', '" . $origine . "')
          ")
        ) {
            return true;
        } else {
            return false;
        }
    }

    public function isDebug()
    {
        return $this->debug;
    }

    public function loadArray($arrayFromFile)
    {
        if (is_null($this->_currentArray)) {

            $this->_currentArray = $arrayFromFile;
        }
    }

    public function isInArray($ipToTest)
    {
        if (!is_null($this->_currentArray)) {
            if (isset($this->_currentArray[$ipToTest])) {
                return true;
            } else {
                return false;
            }
        } else
            return false;
    }
    
    public function isInTempArray($ipToTest)
    {
        if (is_array($this->_tempArray)) {
            if (isset($this->_tempArray[$ipToTest])) {
                return true;
            } else {
                return false;
            }
        } else
            return false;
    }
    public function checkIfTableExist()
    {
        if (false == $this->_linkDb->query("SELECT 1 FROM " . self::CLOAKED_IP_TABLE)) {
            return false;
        } else {
            return true;
        }
    }

    public function purgeTable()
    {
        $this->_linkDb->query('TRUNCATE ' . self::CLOAKED_IP_TABLE);
    }

    public function getTableName()
    {
        return self::CLOAKED_IP_TABLE;
    }

    public function testIfTableEmpty()
    {
        $result = $this->_linkDb->query("SELECT * FROM " . self::CLOAKED_IP_TABLE);
        if (mysqli_num_rows($result) > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function refreshTable($pool = self::DEFAULT_POOL)
    {
        $testForDb = new cloaking();
        try {
            $result = $this->_linkDb->query("SELECT * FROM " . self::CLOAKED_IP_TABLE . " WHERE last_check < DATE_ADD(NOW(), INTERVAL " . self::REFRESH_DAYS_PERIOD . " DAY) ORDER BY last_check ASC LIMIT " . $pool);
        } catch (Exception $e) {
            echo 'Exception reçue : ', $e->getMessage(), "\n";
            "Database Error [{$this->database->errno}] $this->database->error}";
        }
        while ($row = $result->fetch_assoc()) {
            $newHost = $testForDb->getDnsName($row['ip_address_v4']);

            if ($testForDb->checkDnsName($newHost)) {
                if ($newHost != $row['dns_name']) {
                    $sqlStatusUpdate = ", status=1";
                    $sqlDnsUpdate = ", dns_name='" . $newHost . "'";
                    $sqlBotUpdate = "";
                } else {
                    if ($row['bot'] != 'google') {
                        $sqlBotUpdate = ", bot='UNKNOWN'";
                    } else {
                        $sqlBotUpdate = "";
                        $sqlLastUpdate = "";
                    }
                    $sqlStatusUpdate = "";
                    $sqlDnsUpdate = "";
                    $sqlLastUpdate = "";
                }

                $sqlLastUpdate = ", date_update=NOW()";
                $this->newInformation = true;

            } else {
                $sqlStatusUpdate = ", status=0";
                $sqlBotUpdate = ", bot='UNKNOWN'";
                $sqlLastUpdate = ", date_update=NOW()";
                $sqlDnsUpdate = "";
            }

            $request = "UPDATE " . self::CLOAKED_IP_TABLE . " SET last_check=NOW()" . $sqlLastUpdate . $sqlBotUpdate . $sqlStatusUpdate . $sqlDnsUpdate . " WHERE ip_address_v4 ='" . $row['ip_address_v4'] . "'";
            try {
                $this->_linkDb->query($request);
            } catch (Exception $e) {
                echo 'Exception reçue : ', $e->getMessage(), "\n";
                "Database Error [{$this->database->errno}] $this->database->error}";
            }


        }
    }

    public function hasToBeUpdated()
    {
        return $this->newInformation;
    }

    public function genFile($force = false)
    {
        if ($force == true || $this->newInformation == true) {
            $contentFile = '<?php' . "\r\n" . '$ipRange=array(';
            $first = true;
            $result = $this->_linkDb->query("SELECT * FROM " . self::CLOAKED_IP_TABLE . " WHERE status=1 AND bot='google'");
            while ($row = $result->fetch_assoc()) {
                if (!$first) {
                    $contentFile .= ",";
                }
                $contentFile .= '"' . $row['ip_address_v4'] . '"=> true';
                $first = false;
            }
            $contentFile .= ');';

            $ipFileList = fopen(__DIR__ . "/../".self::CACHE_FILES_DIRECTORY."/". self::LIST_IP_FILENAME, "w+");
            fwrite($ipFileList, $contentFile);
            fclose($ipFileList);
        }
    }

    public function addToTempFile($adresseIp)
    {
        if (!filter_var(trim($adresseIp), FILTER_VALIDATE_IP) === false) {
        $newContent = trim($adresseIp) . "\r\n";
            file_put_contents($this->getTempFileUri(), $newContent, FILE_APPEND | LOCK_EX);
        }
    }

    public function isTempFileExisting()
    {
        return file_exists($this->getTempFileUri());
    }

    public function getTempFileUri()
    {
        return __DIR__."/../".self::CACHE_FILES_DIRECTORY."/". self::LIST_IP_TEMP_FILE;
    }

    public function loadTempArray()
    {
        $handle = fopen($this->getTempFileUri(), 'r');
        $tempArray = array();
        while (!feof($handle)) {
            $tempArray[trim(fgets($handle))] = true;
        }
        $this->_tempArray=$tempArray;
        if (count($this->_tempArray) > 0)
            return true;
        else
            return false;
    }

    
}