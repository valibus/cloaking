<?php
/**
 * Created by PhpStorm.
 * User: pvali
 * Date: 28/04/2017
 * Time: 11:18
 */
ini_set('display_errors', 1);
require_once(__DIR__."/framework/framework.php");
//Create table in database
$install = new ipList($linkDb);
echo "<h1>SETUP</h1>";

echo "Test if table already exist : ";
if(!$install->checkIfTableExist()) {

    $linkDb->query("
    CREATE TABLE `" . $install->getTableName() . "` (
    `id` int(11) NOT NULL,
      `ip_address_v4` varchar(15) NOT NULL,
      `dns_name` varchar(255) NOT NULL,
      `date_discover` datetime NOT NULL,
      `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      `last_check` datetime NOT NULL,
      `status` tinyint(1) DEFAULT '0',
      `bot` varchar(30) NOT NULL,
      `origin` varchar(30) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

    $linkDb->query("ALTER TABLE `" . $install->getTableName() . "`
  ADD PRIMARY KEY (`id`);");

    $linkDb->query("ALTER TABLE `" . $install->getTableName() . "`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;");
    echo "Data table ".$install->getTableName()." has been created <br/>";
}else{
    echo "Table already exist, start data integration<br>";
}
if(!$install->testIfTableEmpty()) {
    $install->purgeTable();
    echo "Data table ".$install->getTableName()." has been truncated <br/>";
}
$i=0;
foreach ($ipRange as $ip => $value) {
    if($install->addItemToDb($ip, "install", "google"))
        $i++;
}
echo $i." ip Address has been added to data table ".$install->getTableName()."<br/>";
echo "<b>Setup done.</b>";