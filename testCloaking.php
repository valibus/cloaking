<?php
ini_set('display_errors', 1);

require_once(__DIR__ . "/framework/framework.php");
echo "<h1>TEST CLOAKING</h1>";
echo "<hr><h2>TEST without CACHE</h2>";

echo "<h3>test 1 :  current UA and IP </h3>";
$test1 = new cloaking($_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], true);
echo "is it google: ";
if ($test1->checkIfCloakingOk($cache = false))
    echo " YES";
else
    echo " NO";

echo "<h3>test 2 :  current UA and google IP </h3>";
$test2 = new cloaking("203.208.60.249", $_SERVER['HTTP_USER_AGENT'], true);
echo "is it google: ";
if ($test2->checkIfCloakingOk($cache = false))
    echo " YES";
else
    echo " NO";

echo "<h3>test 3 :  google UA and current IP </h3>";
$test3 = new cloaking($_SERVER['REMOTE_ADDR'], "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)", true);
echo "is it google: ";
if ($test3->checkIfCloakingOk($cache = false))
    echo " YES";
else
    echo " NO";

echo "<h3>test 4 :  google UA and google IP </h3>";
$test4 = new cloaking("203.208.60.249", "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)", true);
echo "is it google: ";
if ($test4->checkIfCloakingOk($cache = false))
    echo " YES";
else
    echo " NO";


echo "<hr><h2>TEST WITH CACHE</h2>";

echo "<h3>test 5 :  current UA and IP </h3>";
$test5 = new cloaking($_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], true);
echo "is it google: ";
if ($test5->checkIfCloakingOk($cache = true, $linkDb, $ipRange))
    echo " YES";
else
    echo " NO";

echo "<h3>test 6 :  current UA and google IP </h3>";
$test6 = new cloaking("203.208.60.249", $_SERVER['HTTP_USER_AGENT'], true);
echo "is it google: ";
if ($test6->checkIfCloakingOk($cache = true, $linkDb, $ipRange))
    echo " YES";
else
    echo " NO";

echo "<h3>test 7 :  google UA and current IP </h3>";
$test7 = new cloaking($_SERVER['REMOTE_ADDR'], "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)", true);
echo "is it google: ";
if ($test7->checkIfCloakingOk($cache = true, $linkDb, $ipRange))
    echo " YES";
else
    echo " NO";

echo "<h3>test 8 :  google UA and google IP </h3>";
$test8 = new cloaking("203.208.60.249", "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)", true);
echo "is it google: ";
if ($test8->checkIfCloakingOk($cache = true, $linkDb, $ipRange))
    echo " YES";
else
    echo " NO";


echo "<hr><h2>TEST WITH TEMP FILE</h2>";
echo "<h3>test 9 :  Add temp IP to file </h3>";
$test9 = new ipList($linkDb);
$test9->addToTempFile("1.1.1.1\r\n");

echo "<h3>test 10 :  google UA and google IP in test file only </h3>";
$test10 = new cloaking("1.1.1.1", "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)", true);
echo "is it google: ";
if ($test10->checkIfCloakingOk($cache = true, $linkDb, $ipRange))
    echo " YES";
else
    echo " NO";