<?php
$b=microtime(true);
ini_set('display_errors', 1);

require_once(__DIR__ . "/../framework/framework.php");

echo "<h1>TEST CLOAKING</h1>";
echo "<hr><h2>TEST without CACHE</h2>";

echo "<h3>test 1 :  current UA and IP </h3>";
$a=microtime(true);
$test1 = new cloaking($_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], true);
echo "is it google: ";
if ($test1->checkIfCloakingOk(false))
    echo " YES";
else
    echo " NO";
echo "<br />".(microtime(true)-$a)." sec <br />";

echo "<h3>test 2 :  current UA and google IP </h3>";
$a=microtime(true);
$test2 = new cloaking("203.208.60.249", $_SERVER['HTTP_USER_AGENT'], true);
echo "is it google: ";
if ($test2->checkIfCloakingOk(false))
    echo " YES";
else
    echo " NO";
echo "<br />".(microtime(true)-$a)." sec <br />";

echo "<h3>test 3 :  google UA and current IP </h3>";
$a=microtime(true);
$test3 = new cloaking($_SERVER['REMOTE_ADDR'], "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)", true);
echo "is it google: ";
if ($test3->checkIfCloakingOk(false))
    echo " YES";
else
    echo " NO";
echo "<br />".(microtime(true)-$a)." sec <br />";

echo "<h3>test 4 :  google UA and google IP </h3>";
$a=microtime(true);
$test4 = new cloaking("203.208.60.249", "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)", true);
echo "is it google: ";
if ($test4->checkIfCloakingOk(false))
    echo " YES";
else
    echo " NO";
echo "<br />".(microtime(true)-$a)." sec <br />";

echo "<br />".(microtime(true)-$b)." final sec <br />";