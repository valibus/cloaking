<?php
ini_set('display_errors', 1);
$b=microtime(true);
require_once(__DIR__ . "/../framework/framework.php");


echo "<hr><h2>TEST WITH CACHE</h2>";

echo "<h3>test 5 :  current UA and IP </h3>";
$a=microtime(true);
$test5 = new cloaking($_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], true);
echo "is it google: ";
if ($test5->checkIfCloakingOk(true, $linkDb, $ipRange))
    echo " YES";
else
    echo " NO";
echo "<br />".(microtime(true)-$a)." sec <br />";

echo "<h3>test 6 :  current UA and google IP </h3>";
$a=microtime(true);
$test6 = new cloaking("203.208.60.249", $_SERVER['HTTP_USER_AGENT'], true);
echo "is it google: ";
if ($test6->checkIfCloakingOk(true, $linkDb, $ipRange))
    echo " YES";
else
    echo " NO";
echo "<br />".(microtime(true)-$a)." sec <br />";

echo "<h3>test 7 :  google UA and current IP </h3>";
$a=microtime(true);
$test7 = new cloaking($_SERVER['REMOTE_ADDR'], "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)", true);
echo "is it google: ";
if ($test7->checkIfCloakingOk(true, $linkDb, $ipRange))
    echo " YES";
else
    echo " NO";
echo "<br />".(microtime(true)-$a)." sec <br />";

echo "<h3>test 8 :  google UA and google IP </h3>";
$a=microtime(true);
$test8 = new cloaking("203.208.60.249", "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)", true);
echo "is it google: ";
if ($test8->checkIfCloakingOk(true, $linkDb, $ipRange))
    echo " YES";
else
    echo " NO";
echo "<br />".(microtime(true)-$a)." sec <br />";

echo "<br />".(microtime(true)-$b)." final sec <br />";