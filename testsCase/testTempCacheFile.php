<?php
ini_set('display_errors', 1);

require_once(__DIR__ . "/../framework/framework.php");

echo "<hr><h2>TEST WITH TEMP FILE</h2>";
echo "<h3>test 9 :  Add temp IP to file </h3>";
$test9 = new ipList($linkDb);
$test9->addToTempFile("1.1.1.1\r\n");

echo "<h3>test 10 :  google UA and google IP in test file only </h3>";
$test10 = new cloaking("1.1.1.1", "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)", true);
echo "is it google: ";
if ($test10->checkIfCloakingOk($cache = true, $linkDb, $ipRange))
    echo " YES";
else
    echo " NO";